from banking_scrape.core import CoreTools
from banking_scrape import security
from banking_scrape.db_connect import DatabaseInterface
from banking_scrape.todo_log import ToDoLog
import logging

import banking_scrape.banks.ajbell
import banking_scrape.banks.bos
import banking_scrape.banks.coventry
import banking_scrape.banks.nationwide
import banking_scrape.banks.natwest
import banking_scrape.banks.paypal
import banking_scrape.banks.paypal2
import banking_scrape.banks.skrill
import banking_scrape.banks.slc
import banking_scrape.banks.tesco
import banking_scrape.banks.tsb
import banking_scrape.banks.virgin
import banking_scrape.banks.zopa


logging.basicConfig(filename='banking_debug.log', level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')


class BankScraper:
    def __init__(self, accounts=[], audit=''):
        self.audit = audit
        self.accounts = accounts
        self.accountIDs = []
        self.next_bank_list = []
        self.account_tuple = ()
        self.account_balances = []

        # An instance of each class
        self.MyDB = DatabaseInterface()
        self.Core = CoreTools()
        self.ToDo = ToDoLog(audit=self.audit)

        # Stores the fail count in a dictionary for each account. Reads the max value found in it.
        self.fail_count_dict = {0: 0}
        self.fail_count = max(self.fail_count_dict.values())
        self.fail_count_limit = 2  # Adjust to make the script stricter.
        self.failed = set()  # Uses a set so values are unique
        self.fail_sleep_time = 30

        self.generate_log()
        self.process_log()

    def generate_log(self):
        """ Generates the to-do file."""

        if self.audit == 'new':
            self.accounts = self.MyDB.get_all_script_names()

        # Add all accounts to the work log.
        if self.accounts:
            for script_name in self.accounts:
                self.ToDo.append_to_log(script_name)

    def process_log(self):
        # Continue while there are still banks and there haven't been too many fails.
        while self.ToDo.log_length != 0 and self.fail_count < self.fail_count_limit:
            self.check_fails()

            # Read the log. If it is empty, break, otherwise login + scrape.
            self.ToDo.read_next()
            if self.ToDo.next_bank == "":
                logging.info('No more accounts. Breaking.')
                break

            self.get_login_information()
            self.login_and_scrape()

        if self.audit and self.fail_count < self.fail_count_limit:
            logging.info('Audit complete. Calculating total.')
            self.MyDB.calculate_cumulative()
        elif self.audit and self.fail_count > self.fail_count_limit:
            logging.error('Audit incomplete. Check logs.')
        else:
            logging.info('Script finished.')

    def check_fails(self):
        """ Checks how many fails have happened so far."""
        self.fail_count = max(self.fail_count_dict.values())
        for site in self.fail_count_dict.items():
            if site[1] > 0:
                self.failed.add(site)
        if self.failed:
            logging.warning(f'Current fails - {self.failed}')
            logging.info(f'Max fail count currently {self.fail_count}')

    def get_login_information(self):
        """ Takes script name and gets account information."""
        self.next_bank_list = self.MyDB.get_specific_site_by_script_name(self.ToDo.next_bank)
        self.accountIDs = []
        for i in range(0, len(self.next_bank_list)):
            self.accountIDs.append(self.next_bank_list[i][2])
        self.account_tuple = security.decrypt_tuple(self.next_bank_list[0][1])

    def login_and_scrape(self):
        """ Logs in to the bank, scrapes balance and writes to the DB. """
        logging.info(f'Loading {self.ToDo.next_bank}')
        try:
            # Try to run the main function and scrape the balance.
            self.account_balances = eval(self.ToDo.next_bank).main(self, self.account_tuple)
            if self.account_balances:
                for i in range(0, len(self.account_balances)):
                    self.MyDB.update_balance_by_account_id(self.account_balances[i], self.accountIDs[i])
                self.ToDo.pop_bank(self.ToDo.next_bank)
        except Exception as e:
            """ Keeps track of errors. """
            logging.error(f'Error {e} with {self.ToDo.next_bank}')
            # Push the script to the end of the list.
            self.ToDo.push_failed_to_end(self.ToDo.next_bank)
            # Get the current fail count.
            account_fail_count = self.fail_count_dict.get(self.ToDo.next_bank, 0)
            account_fail_count += 1
            self.fail_count_dict[self.ToDo.next_bank] = account_fail_count
            logging.error(f'{self.ToDo.next_bank} has now failed {account_fail_count} times.')
            self.Core.sleep(self.fail_sleep_time, 'resting after failing.')


def main(accounts=[], audit=''):
    if audit:
        logging.info('Running an audit.')
    MyScraper = BankScraper(accounts, audit)


# This is for quick debugging.
#main(accounts=['banking_scrape.banks.slc'])
main(audit='new')
#main(audit='continue')