"""
Creates an interface for interacting with the Database.
The login tuple should be in the form ['bank name', ('username', 'password'), 'script name')

"""

import psycopg2
import logging
from banking_scrape import security


logging.basicConfig(filename='banking_debug.log', level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')


class DatabaseInterface:

    def __init__(self):
        self.db_name = 'project_scrape'
        self.username = 'obow'
        self.host = 'localhost'
        self.password = security.get_password_from_keyring(self.username, 'PostgreSQL')
        self.schema = 'banking'
        self.db, self.cur = self.connect()
        self.sites = []
        self.results = []

    def connect(self):
        """ Connects. The options section is to control schema. """
        self.db = psycopg2.connect(f"dbname='{self.db_name}' "
                                   f"user='{self.username}' "
                                   f"host='{self.host}' "
                                   f"password='{self.password}' "
                                   f"options='-c search_path={self.schema}'")
        return self.db, self.db.cursor()

    def get_all_site_details(self):
        """ Returns the details of every bank in the database."""
        get_all_sql = f"SELECT banks.bankname, banks.logininformation, banks.scriptname FROM banks INNER JOIN accounts ON banks.id=accounts.bankid ORDER BY accounts.accountid;"
        self.cur.execute(get_all_sql)
        self.results = self.cur.fetchall()
        return self.results

    def get_all_script_names(self):
        """ Returns the script name of every bank in the database. """
        get_all_ids_sql = f'SELECT banks.scriptName FROM banks ORDER BY id;'
        self.cur.execute(get_all_ids_sql)
        self.sites = []
        for i in self.cur.fetchall():
            self.sites.append(i[0])
        return self.sites

    def get_specific_site_by_script_name(self, script_name):
        """ Returns a specific site based on script name. I store login by bank rather than by account. """
        login_SQL = f"SELECT banks.bankname, banks.logininformation, accounts.accountid FROM banks INNER JOIN banking.accounts ON banks.id=accounts.bankid WHERE banks.scriptname = '{script_name}' ORDER BY accounts.accountid;"
        self.cur.execute(login_SQL)
        return self.cur.fetchall()

    def update_balance_by_account_id(self, balance, accountid):
        """ Updates the balance for an account by ID. """
        update_balance_sql = f"UPDATE accounts SET balance = {balance}, lastaudited = CURRENT_DATE WHERE accountid = {accountid};"
        self.cur.execute(update_balance_sql)
        self.commit()

    def insert_new_login(self, bank_name, encrypted_login_information, script_name):
        """ Executes the SQL to insert a new login. """
        insert_SQL = f"INSERT INTO banks (bankname, logininformation, scriptname) VALUES ('{bank_name}', '{encrypted_login_information}', '{script_name}');"
        logging.info('Inserting new login')
        self.cur.execute(insert_SQL)
        self.commit()

    def update_login_by_script_name(self, encrypted_login_information, script_name):
        """ Executes the SQL to update login information. """
        insert_SQL = f"UPDATE banks SET logininformation = '{encrypted_login_information}' WHERE scriptname = '{script_name}';"
        logging.info('Updating new login')
        self.cur.execute(insert_SQL)
        self.commit()

    def manually_add(self, bank_name, login_tuple, script_name):
        """ Used to manually add login details. """
        encrypted_login_information = security.encrypt(login_tuple)
        self.insert_new_login(bank_name, encrypted_login_information, script_name)

    def manually_update(self, login_tuple, script_name):
        """ Used to manually update login details. """
        encrypted_login_information = security.encrypt(login_tuple)
        self.update_login_by_script_name(encrypted_login_information, script_name)

    def calculate_cumulative(self):
        """ Run after an audit. Sums the accounts and inserts into HistoricalBalance"""
        cumulative_SQL = 'INSERT INTO HistoricalBalance (AuditDate, CumulativeCredit, CumulativeDebt) VALUES (CURRENT_DATE, (SELECT SUM(Balance) FROM Accounts WHERE Positive IS TRUE), (SELECT SUM(Balance) FROM Accounts WHERE Positive IS NOT TRUE));'
        update_SQL = 'UPDATE HistoricalBalance SET CumulativeBalance = (SELECT (CumulativeCredit - CumulativeDebt));'
        self.cur.execute(cumulative_SQL)
        self.cur.execute(update_SQL)
        self.commit()

    def commit(self):
        """ Commits changes. """
        self.db.commit()


#myDB = DatabaseInterface()
