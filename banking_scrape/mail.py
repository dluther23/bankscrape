"""

Used specifically for Tesco at the moment, but wouldn't need much tweaking to adjust that.

"""

from email.parser import BytesParser
import imaplib
import re
import datetime
import logging
from banking_scrape import core, security

logging.basicConfig(filename='banking_debug.log', level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')

GMAIL_USERNAME = 'd.luther@gmail.com'


def login(username, password):
    """ Logs in to email account. """
    mail = imaplib.IMAP4_SSL('imap.gmail.com')
    mail.login(username, password)
    return mail


def retrieve_last_email_id(mail, mail_tag, from_who):
    """ Searches for the tag and sender, retrieves the ID for the latest email. """
    mail.select(mail_tag)
    message_type, data = mail.search(None, f'(HEADER From {from_who})')
    mail_ids = data[0]
    try:
        last_email_id = mail_ids.split()[-1]
        return last_email_id
    except IndexError:
        pass


def get_code_from_id(mail, email_id):
    """ Fetches the text of the requested email and extracts code. """ 
    data = mail.fetch(email_id, '(BODY[TEXT])')
    contents = data[1][0][1]
    parser = BytesParser()
    msg = parser.parsebytes(contents).as_string()
    code = number_from_string(msg)
    return code


def get_senddate__from_id(mail, email_id):
    """ Fetches the header. Used to check the email being checked is recent enough. """
    data = mail.fetch(email_id, '(BODY[HEADER])')
    header = data[1][0][1]
    parser = BytesParser()
    date_from_header = parser.parsebytes(header).get('Date')
    return date_from_header


def compare_time_to_now(string_date):
    t = datetime.datetime.strptime(string_date, '%a, %d %b %Y %H:%M:%S')
    now = datetime.datetime.now().strftime('%a, %d %b %Y %H:%M:%S')
    now = datetime.datetime.strptime(now, '%a, %d %b %Y %H:%M:%S')
    # Calculates the difference between now and when the message was sent. 
    difference = now - t
    # Converts it to seconds for maths.
    seconds = difference.seconds
    if seconds > 500:
        return 1    # Too old
    else:
        return 0    # New enough


def get_tesco_code():
    GMAIL_PWD = security.get_password_from_keyring(GMAIL_USERNAME, "My GMail")
    if not GMAIL_PWD:
        logging.error('No Password found. ')
        return
    mail_tag = '"SMS Backup"'
    from_who = 'TescoBank'
    email_id = None
    counter = 0
    core.sleep(60, "waiting for SMS to eMail app.")
    while email_id is None and counter < 60:
        mail = login(GMAIL_USERNAME, GMAIL_PWD)
        email_id = retrieve_last_email_id(mail, mail_tag, from_who)
        if email_id is None:
            logging.info('No email found.')
            core.sleep(5, 'waiting for email.')
            counter += 1
            continue
        # List splice used to remove timezone information
        date_from_header = get_senddate__from_id(mail, email_id)[:-6]
        if compare_time_to_now(date_from_header) == 1:
            logging.info('Message too old.')
            core.sleep(10, 'waiting for email.')
            counter += 1
            email_id = None
            continue
        else:
            code = get_code_from_id(mail, email_id)
            return code


def number_from_string(string_in):
    """ Takes a string, returns the first digit. """
    # d+ means 10. d would split it into 1 and 0 """
    regex = re.compile(r'(\d+)')
    found = (regex.findall(string_in))
    logging.info(f'Extracted {found[0]} from {string_in}.')

    return found[0]
