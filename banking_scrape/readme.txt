Bank Scraper

Overview.
A Python script that logs into bank accounts, scrapes the balance and saves to a database.


Setup.
It was written with Python 3.7. It needs the following modules installed:
cryptography - Encrypt passwords securely.
keyring - Access OS keyring.
psycopg2-binary - PostgreSQL interface.
selenium - Webscraping

It was written with a PostgreSQL backend. It isn't hard to change to a different backend.

I use the software SMS Backup on my phone so I can read the SMS that Tesco send me.

There are some values hardcoded into the scripts. They need changing to meet your environment.
1. db_connect.py - the values in __init__ 
2. mail.py - This was written with GMail in mind. If you're not using GMail then you'll need to edit login(). Has my username hardcoded in.
3. security.py - You'll need to use generate_key() to generate a Fernet key to encrpyt your database login information. You should use set_password_in_keyring() to store your eMail and DB passwords, but you can hardcode them in if you like.



You will then have to use db_connect.py manuaully_add() to add your own login information to your database. One way to do this, is to use db_connect.py.

For example to add 
myDB = DatabaseInterface()
myDB.manually_add('Paypal', ('myusername', 'mypassword'), 'banks.paypal')

It will be automatically encrypted and saved to the database.

The information should be in the form
['bank name', ('username', 'password'), 'script name']

For websites like Zopa where there are security questions to answer, the data is stored in a dictionary. Then the format is...
['bank name', ('username', 'password', {'Question 1': 'Answer 1', 'Question 2': 'Answer 2', 'Question 3': 'Answer 3'}), 'script name']



The SQL I used to create my tables in PostgreSQL is:

create table banking.accounts
(
  accountid      serial  not null
    constraint idx_16814_primary
    primary key,
  bankid         integer not null
    constraint accounts_ibfk_1
    references banks
    on update restrict on delete restrict,
  balance        numeric(12, 2),
  accountdetails varchar(100),
  positive       boolean,
  lastaudited    date
);

create index idx_16814_bankid
  on banking.accounts (bankid);



create table banking.banks
(
  id                  serial                                       not null
    constraint idx_16820_primary
    primary key,
  bankname            varchar(100) default '' :: character varying not null,
  oldlogininformation bytea,
  scriptname          varchar(100),
  logininformation    text
);



create table banking.historicalbalance
(
  auditid           bigserial not null
    constraint idx_16830_primary
    primary key,
  auditdate         date,
  cumulativecredit  numeric(12, 2),
  cumulativedebt    numeric(12, 2),
  cumulativebalance numeric(12, 2)
);


Change log.

v0.1 - First release.
v0.11 - Inspected code and made it more Pythonic.
v0.14 - Fixed imports.
v0.15 - Minor tweak to overview, added some commented lines for testing.
v0.16 - Added support for Coventry.
v0.17 - Adjusted code. Learnt about assigning multiple variables from a list at the same time.