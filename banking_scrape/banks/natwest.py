"""
Logs into Natwest

A pain in the arse.
Will fail if there are messages to read after login.

As it was a pain, it's the worst script. I got it working and left it there. Could defintely be improved.

"""

from selenium.webdriver.common.keys import Keys
import logging

logging.basicConfig(filename='banking_debug.log', level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')


def read_characters_needed(BankScraper, css_element):
    pin_needed = []
    for i in range(0, 3):
        pin_element = BankScraper.Core.driver.find_elements_by_css_selector(css_element)[i]
        pin_needed.append(BankScraper.Core.number_from_string(pin_element.text))

    password_needed = []
    for i in range(3, 6):
        password_element = BankScraper.Core.driver.find_elements_by_css_selector(css_element)[i]
        password_needed.append(BankScraper.Core.number_from_string(password_element.text))

    return pin_needed, password_needed


def main(BankScraper, login_tuple):
    """ """
    URL = 'https://www.nwolb.com/Login.aspx'

    customer_number, pin, memorable = login_tuple

    frame_element_id = '[id="ctl00_secframe"]'
    customer_number_element_css = '#ctl00_mainContent_LI5TABA_CustomerNumber_edit'
    pin_elements_css = '[for^="ctl00_mainContent_Tab1_LI"]'
    pin_input_elements_css = ['#ctl00_mainContent_Tab1_LI6PPEA_edit',
                                      '#ctl00_mainContent_Tab1_LI6PPEB_edit', '#ctl00_mainContent_Tab1_LI6PPEC_edit']
    password_input_elements_css = ['#ctl00_mainContent_Tab1_LI6PPED_edit',
                                           '#ctl00_mainContent_Tab1_LI6PPEE_edit',
                                           '#ctl00_mainContent_Tab1_LI6PPEF_edit']
    login_element_css = '[type="submit"]'
    checkbox_css = '[type="checkbox"]'
    balance_elements_css = ['#Account_E12557C64FC31CCFA776F42E63204541F8F6A6A3 > td:nth-child(4)']

    BankScraper.Core.start_and_open(URL)
    BankScraper.Core.driver.switch_to.frame(
        BankScraper.Core.driver.find_element_by_css_selector(frame_element_id))
    # I think it's like this as it works even through the popup that sometimes appears.
    userbox = BankScraper.Core.driver.find_element_by_css_selector(customer_number_element_css)
    userbox.send_keys(customer_number)
    userbox.send_keys(Keys.ENTER)

    # Scrape PIN digits needed, return and input them.
    pin_digits_needed, password_digits_needed = read_characters_needed(BankScraper, pin_elements_css)
    pin_inputs = BankScraper.Core.get_memorable_characters(pin, pin_digits_needed)
    for i in range(0, 3):
        pin_input_element = BankScraper.Core.driver.find_element_by_css_selector(pin_input_elements_css[i])
        pin_input_element.send_keys(pin_inputs[i])

    # Scrape password digit needed, return and input them.
    password_inputs = BankScraper.Core.get_memorable_characters(memorable, password_digits_needed)
    for i in range(0, 3):
        password_input_element = BankScraper.Core.driver.find_element_by_css_selector(
            password_input_elements_css[i])
        password_input_element.send_keys(password_inputs[i])

    BankScraper.Core.click_element(login_element_css)

    try:
        BankScraper.Core.sleep(1)
        BankScraper.Core.click_element(checkbox_css)
        BankScraper.Core.click_element(login_element_css)
    except:
        logging.info("No checkbox.")

    account_balance = BankScraper.Core.scrape_balances(balance_elements_css)

    BankScraper.Core.driver.quit()

    return account_balance
