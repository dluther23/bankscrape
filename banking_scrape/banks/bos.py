"""
Logs into Bank of Scotland + Halifax and scrapes.

Username, password, memorable information in dropdowns.
"""

import logging

logging.basicConfig(filename='banking_debug.log', level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')


def scrape_memorable_info(BankScraper, memorable_element_css, memorable_info):
    """ Finds the digits needed from memorable information. """
    memorable_text = BankScraper.Core.text_from_element(memorable_element_css)
    memorable_input = BankScraper.Core.get_memorable_characters(
        memorable_info, BankScraper.Core.numbers_from_string(memorable_text))
    return memorable_input


def main(BankScraper, login_tuple):
    """ """
    URL = 'https://online.bankofscotland.co.uk/personal/logon/login.jsp'

    username, password, memorable = login_tuple

    username_element_css = '[id="frmLogin:strCustomerLogin_userID"]'
    password_element_css = '[id="frmLogin:strCustomerLogin_pwd"]'
    first_continue_element_css = '[name="frmLogin:btnLogin1"]'
    memorable_element_css = '#page > div.primaryWrap > div > div > div > p:nth-child(2) > strong > strong'
    dropdown_elements_css = ['[id="frmentermemorableinformation1:strEnterMemorableInformation_memInfo1"]',
                                 '[id="frmentermemorableinformation1:strEnterMemorableInformation_memInfo2"]',
                                 '[id="frmentermemorableinformation1:strEnterMemorableInformation_memInfo3"]']
    second_continue_element_css = '[id="frmentermemorableinformation1:btnContinue"]'
    intermittent_continue_css = '[value="Continue"]'
    balance_elements_css = [
        '#des-m-sat-xx-1 > div > div.des-m-sat-xx-account-information > div:nth-child(1) > p.balance.ManageMyAccountsAnchor2',
        '#des-m-sat-xx-2 > div > div.des-m-sat-xx-account-information > div:nth-child(1) > p.balance.ManageMyAccountsAnchor2']

    BankScraper.Core.start_and_open(URL)
    # Inputs username + password then clicks continue.
    BankScraper.Core.input_to_element(username_element_css, username)
    BankScraper.Core.input_to_element(password_element_css, password)
    BankScraper.Core.click_element(first_continue_element_css)

    # Works out memorable input.
    memorable_input = scrape_memorable_info(BankScraper, memorable_element_css, memorable)

    # Selects memorable information in dropdowns then continues.
    for i in range(0, len(memorable_input)):
        BankScraper.Core.select_visible_from_element(dropdown_elements_css[i], f" {memorable_input[i]}")
    BankScraper.Core.click_element(second_continue_element_css)
    BankScraper.Core.sleep(1)

    try:
        logging.info("Checking for sometimes continue offer.")
        BankScraper.Core.click_element(intermittent_continue_css)
    except:
        logging.info("No intermittent continue box.")

    BankScraper.Core.sleep(1)

    # Scrapes, quits + returns.
    account_balances = BankScraper.Core.scrape_balances(balance_elements_css)
    BankScraper.Core.driver.quit()

    return account_balances
