"""
Logs into Nationwide and scrapes.

Customer number, picks memorable data, uses some javascript to make dropdowns visible and then inputs memorable data.

"""

from selenium.webdriver.support.ui import Select
import logging

logging.basicConfig(filename='banking_debug.log', level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')


def nw_dropdown_input(BankScraper, elements_list, javascript_commands, memorable_input):
    for i in range(0, 3):
        BankScraper.Core.driver.execute_script(javascript_commands[i])
        Select(elements_list[i]).select_by_visible_text(f"{memorable_input[i]}")


def main(BankScraper, login_tuple):
    """ """
    URL = 'https://onlinebanking.nationwide.co.uk/AccessManagement/Login'

    customer_number, pass_number, memorable_data = login_tuple

    customer_number_element_css = '[data-pl-bind-value="CustomerNumber"]'
    memorable_choice_element_css = 'div.control__input > div > div:nth-child(2) > label'
    memorable_input_element_css = '[name="MemorableData"]'
    memorable_elements_css = ['div.control.control--complex.passnumber-partial-entry.control--enabled.control'
                                 '--loaded > div.control__input > div:nth-child(1) > label > span',
                                 'div.control.control--complex.passnumber-partial-entry.control--enabled.control--loaded > div.control__input > div:nth-child(2) > label > span',
                                 'div.control.control--complex.passnumber-partial-entry.control--enabled.control--loaded > div.control__input > div:nth-child(3) > label > span']
    continue_button = '[class="action__button"]'
    dropdown_element_css = '[class="selection-list__options"]'
    overview_url = 'https://onlinebanking.nationwide.co.uk/AccountList/AccountList/Index'
    accounts_css = [
        'table:nth-child(6) > tbody > tr:nth-child(2) > td > form > table > tbody > tr.account-row > td.bal-details',
        'table:nth-child(6) > tbody > tr:nth-child(3) > td > form > table > tbody > tr.account-row.non-first > td.bal-details',
        'table:nth-child(7) > tbody > tr:nth-child(2) > td > form > table > tbody > tr.account-row > td.bal-details']
    javascript_commands = [
        "document.getElementsByClassName('selection-list__value')[0].setAttribute('aria-hidden', 'false');",
        "document.getElementsByClassName('selection-list__value')[1].setAttribute('aria-hidden', 'false');",
        "document.getElementsByClassName('selection-list__value')[2].setAttribute('aria-hidden', 'false');"]

    # Load site, input number, picks memorable data.
    BankScraper.Core.start_and_open(URL)
    BankScraper.Core.input_to_element(customer_number_element_css, customer_number)
    BankScraper.Core.click_element(memorable_choice_element_css)
    BankScraper.Core.input_to_element(memorable_input_element_css, memorable_data)

    digits_needed = BankScraper.Core.work_out_digits(memorable_elements_css)
    memorable_input = BankScraper.Core.get_memorable_characters(pass_number, digits_needed)

    dropdown_elements = BankScraper.Core.driver.find_elements_by_css_selector(dropdown_element_css)
    nw_dropdown_input(BankScraper, dropdown_elements, javascript_commands, memorable_input)

    BankScraper.Core.click_element(continue_button)

    # Load the page that has all the balances easily accessible.
    BankScraper.Core.driver.get(overview_url)

    # Scrape and return the balances
    account_balances = BankScraper.Core.scrape_balances(accounts_css)
    BankScraper.Core.driver.quit()

    return account_balances
