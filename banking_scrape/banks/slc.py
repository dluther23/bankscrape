"""
Logs into slc
Scrapes balances.
"""

import logging

logging.basicConfig(filename='banking_debug.log',level=logging.INFO, format='%(asctime)s %(levelname)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')


def main(BankScraper, login_tuple):
    """ """
    slc_URL = 'https://secure.studentloanrepayment.co.uk/pls/portal/url/page/rpipg001/rpips003'

    slc_customer_number, slc_password, slc_question_answer = login_tuple

    slc_customer_number_element_css = 'div:nth-child(2) > input[type="text"]'
    slc_next_element_css = 'div.submitc > div.function.next.right > button'
    slc_password_element_css = 'div.formblock.first > div:nth-child(2) > input[type="password"]'
    slc_question_element_css = 'div.formwrap > div > div:nth-child(2) > div:nth-child(2) > input[type="password"]'
    slc_balance_elements_css = ['div.table-wrap > table > tbody > tr:nth-child(2) > td:nth-child(1) > strong']

    BankScraper.Core.start_and_open(slc_URL)

    # Input customer number, click next.
    BankScraper.Core.input_to_element(slc_customer_number_element_css, slc_customer_number)
    BankScraper.Core.click_element(slc_next_element_css)

    # Input password + secret question, click next
    BankScraper.Core.input_to_element(slc_password_element_css, slc_password)
    BankScraper.Core.input_to_element(slc_question_element_css, slc_question_answer)
    BankScraper.Core.click_element(slc_next_element_css)

    # Scrape
    account_balance = BankScraper.Core.scrape_balances(slc_balance_elements_css)
    BankScraper.Core.driver.quit()
    
    return account_balance
