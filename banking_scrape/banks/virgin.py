"""
Logs into Virgin
Scrapes balance.
"""

import logging

logging.basicConfig(filename='banking_debug.log', level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')


def main(BankScraper, login_tuple):
    """ """
    virgin_URL = 'https://online.virginmoney.com/SSO/SSO/CustomerIdView.jsf'

    virgin_account_id, virgin_password, virgin_question_answer_dict = login_tuple

    virgin_account_id_element_css = '[id="f:customer-id:itc"]'
    virgin_signin_element_css = '[id="f:btn-signin"]'
    virgin_password_elements_css = ['[for="f:pwd-index-1"]', '[for="f:pwd-index-2"]', '[for="f:pwd-index-3"]']
    virgin_password_input_elements_css = ['[id="f:pwd-index-1"]', '[id="f:pwd-index-2"]', '[id="f:pwd-index-3"]']
    virgin_question_text_element_css = '[for="f:squestion-school:isc"]'
    virgin_continue_element_css = '[id="f:btn-next"]'
    virgin_question_element_css = '[id="f:squestion-school:isc"]'
    virgin_balance_elements_css = ['[class="balance-head"]']

    BankScraper.Core.start_and_open(virgin_URL)
    # Input Account ID + click next
    BankScraper.Core.input_to_element(virgin_account_id_element_css, virgin_account_id)
    BankScraper.Core.click_element(virgin_signin_element_css)

    # Get required digits, input them + click next.
    password_digits_needed = BankScraper.Core.work_out_digits(virgin_password_elements_css)
    password_inputs = BankScraper.Core.get_memorable_characters(virgin_password, password_digits_needed)
    BankScraper.Core.input_to_elements(virgin_password_input_elements_css, password_inputs)
    BankScraper.Core.sleep(1)
    BankScraper.Core.click_element(virgin_continue_element_css)
    BankScraper.Core.sleep(3)

    # Answer security question, click next twice.
    virgin_question_text = BankScraper.Core.text_from_element(virgin_question_text_element_css)
    try:
        virgin_question_answer = virgin_question_answer_dict[virgin_question_text]
    except KeyError:
        logging.error("Unexpected question.")

    BankScraper.Core.input_to_element(virgin_question_element_css, virgin_question_answer)
    BankScraper.Core.click_element(virgin_continue_element_css)
    BankScraper.Core.click_element(virgin_continue_element_css)

    account_balance = BankScraper.Core.scrape_balances(virgin_balance_elements_css)
    BankScraper.Core.driver.quit()

    return account_balance
