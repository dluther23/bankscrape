"""
Logs into AJ Bell
Scrapes balance.

The biggest challenge with AJ Bell is that they generate the element names that you input memorable characters into.
This is why I made generate_elements_from_digits().
The rest is standard. It only has a single 
"""

import logging

logging.basicConfig(filename='banking_debug.log', level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')


def generate_elements_from_digits(list_of_characters, base_text):
    """ Takes the characters needed and creates the required CSS element names"""
    css_elements = []
    for i in list_of_characters:
        base_text_done = base_text.format(i)
        css_elements.append(base_text_done)
    return css_elements


def main(BankScraper, login_tuple):
    """ """
    URL = 'https://www.youinvest.co.uk/securelogin'

    username, password, question_answer = login_tuple

    username_element_css = '#edit-name'
    next_element_css = '#edit-submit'
    password_element_base_css = '[id="edit-password-challenge-challenges-{}"]'
    password_digit_element_css = ['#edit-password-challenge-challenges']
    question_element_css = '#edit-security-answer'
    balance_elements_css = ['#accounts-overview > tfoot > tr > th:nth-child(4)']

    BankScraper.Core.start_and_open(URL)

    # Input email + password, click next
    BankScraper.Core.input_to_element(username_element_css, username)

    BankScraper.Core.click_element(next_element_css)

    # Scrape the needed digits for the password, determine characters to input, generate elements and input characters.
    password_digits_needed = BankScraper.Core.work_out_digits(password_digit_element_css)
    password_inputs = BankScraper.Core.get_memorable_characters(password, password_digits_needed)
    password_input_elements_css = generate_elements_from_digits(password_digits_needed, password_element_base_css)
    BankScraper.Core.input_to_elements(password_input_elements_css, password_inputs)

    # Only one security question on AJ Bell. Answer it, click next.
    BankScraper.Core.input_to_element(question_element_css, question_answer)

    BankScraper.Core.click_element(next_element_css)
    BankScraper.Core.sleep(2, 'loading balance.')

    account_balances = BankScraper.Core.scrape_balances(balance_elements_css)
    BankScraper.Core.driver.quit()

    return account_balances
