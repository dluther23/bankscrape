"""
Logs into Bank of Scotland.
Scrapes balance of Classic Plus Account
"""

import logging

logging.basicConfig(filename='banking_debug.log', level=logging.INFO, format='%(asctime)s %(levelname)s %(message)s',
                    datefmt='%m/%d/%Y %I:%M:%S %p')


def scrape_memorable_info(BankScraper, memorable_element_css, memorable_info):
    """ Finds the digits needed from memorable information. """
    memorable_text = BankScraper.Core.text_from_element(memorable_element_css)
    memorable_input = BankScraper.Core.get_memorable_characters(memorable_info,
                                                                BankScraper.Core.numbers_from_string(memorable_text))
    return memorable_input


def main(BankScraper, login_tuple):
    """ """
    tsb_URL = 'https://internetbanking.tsb.co.uk/personal/logon/login/#/login'

    tsb_username, tsb_password, tsb_memorable = login_tuple

    tsb_username_element_css = '#userIdInput > input'
    tsb_password_element_css = '#passwordInput > input'
    tsb_memorable_element_css = '[class="col-xs-12 text-bold sm-margin-bottom no-padding text-std"]'
    tsb_login_element_css = '[ng-click="submit(loginForm)"]'
    tsb_dropdown_elements_css = ['[id="charXPos"]', '[id="charYPos"]', '[id="charZPos"]']
    tsb_continue_element_css = '[ng-click="submit(memorableInformationForm)"]'
    tsb_intermittent_continue_css = '[value="Continue"]'
    tsb_balance_elements_css = [
        'div > div.col-xs-12.col-sm-11.content > div:nth-child(1) > div.col-xs-12.col-sm-11 > div > div:nth-child(2) > ul > li.list-element.no-bullet.amount > span']

    # Username, password, click.
    BankScraper.Core.start_and_open(tsb_URL)
    BankScraper.Core.input_to_element(tsb_username_element_css, tsb_username)
    BankScraper.Core.input_to_element(tsb_password_element_css, tsb_password)
    BankScraper.Core.click_element(tsb_login_element_css)

    # Scrapes memorable info then selects in dropdowns and clicks continue.
    memorable_input = scrape_memorable_info(BankScraper, tsb_memorable_element_css, tsb_memorable)
    for i in range(0, 3):
        BankScraper.Core.select_visible_from_element(tsb_dropdown_elements_css[i], f"{memorable_input[i]}")
    BankScraper.Core.click_element(tsb_continue_element_css)

    try:
        logging.info("Checking for sometimes continue offer.")
        BankScraper.Core.click_element(tsb_intermittent_continue_css)
    except:
        logging.info("No intermittent continue box.")

    BankScraper.Core.sleep(2, 'waiting for balance to load.')
    account_balances = BankScraper.Core.scrape_balances(tsb_balance_elements_css)
    BankScraper.Core.driver.quit()

    return account_balances
