"""
Logs into zopa
Scrapes balances.
"""

import logging

logging.basicConfig(filename='banking_debug.log', level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')


def main(BankScraper, login_tuple):
    """ """
    zopa_URL = 'https://secure2.zopa.com/login'

    zopa_email, zopa_password, zopa_question_answer_dict = login_tuple

    zopa_email_element_css = '#email'
    zopa_signin_element_css = '#submit-button'
    zopa_password_element_css = '#password'
    zopa_question_text_element_css = '#form > fieldset > div > p > label'
    zopa_question_element_css = '#SecurityAnswer'
    zopa_balance_elements_css = ['#earnings-summary-expanded > div.row.group > div > div:nth-child(4) > div.number']

    BankScraper.Core.start_and_open(zopa_URL)

    # Input email + password, click next
    BankScraper.Core.sleep(5, 'giving Zopa time to load.')
    BankScraper.Core.input_to_element(zopa_email_element_css, zopa_email)
    BankScraper.Core.input_to_element(zopa_password_element_css, zopa_password)
    BankScraper.Core.click_element(zopa_signin_element_css)

    # Read question, lookup answer, input it, click next.
    zopa_question_text = BankScraper.Core.text_from_element(zopa_question_text_element_css)
    zopa_question_answer = zopa_question_answer_dict[zopa_question_text]
    BankScraper.Core.input_to_element(zopa_question_element_css, zopa_question_answer)
    BankScraper.Core.click_element(zopa_signin_element_css)

    account_balance = BankScraper.Core.scrape_balances(zopa_balance_elements_css)
    BankScraper.Core.driver.quit()

    return account_balance
