"""
Logs into Coventry and scrapes.

Enters web ID, date of birth, password characters, grid card, scrapes and logs out.


The password and grid card are both stored as dictionaries. There's surely a more elegant way to do it, but it works.

"""

from selenium.webdriver.support.ui import Select
import logging

logging.basicConfig(filename='banking_debug.log', level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')


def dropdown_input(BankScraper, elements_list, input_list):
    for i in range(0, 3):
        element = BankScraper.Core.driver.find_element_by_css_selector(elements_list[i])
        Select(element).select_by_visible_text(input_list[i])

def main(BankScraper, login_tuple):
    """ """
    URL = 'https://www.coventrybuildingsociety.co.uk/onlineservices/login/ols_login.aspx'

    web_id, date_of_birth, password_dictionary, grid_dict = login_tuple

    web_id_input_element_css = '#ctl00_OLSContent_txtInput99'
    date_of_birth_dropdown_elements_css = ['#ctl00_OLSContent_DateOfBirth1_ddlDay', '#ctl00_OLSContent_DateOfBirth1_ddlMonth', '#ctl00_OLSContent_DateOfBirth1_ddlYear']
    cookies_button_css = '#acceptCookie'
    next_button_css = '#ctl00_OLSContent_aNext'

    password_required_elements_css = ['#ctl00_OLSContent_lblFirst', '#ctl00_OLSContent_lblSecond', '#ctl00_OLSContent_lblThird']
    password_input_box_elements_css = ['#ctl00_OLSContent_txtFirst', '#ctl00_OLSContent_txtSecond', '#ctl00_OLSContent_txtThird']

    grid_required_elements_css = ['#ctl00_OLSContent_GridChallenge1_GridLabel1', '#ctl00_OLSContent_GridChallenge1_GridLabel2', '#ctl00_OLSContent_GridChallenge1_GridLabel3']
    grid_input_box_elements_css = ['#ctl00_OLSContent_GridChallenge1_GridEntry1', '#ctl00_OLSContent_GridChallenge1_GridEntry2', '#ctl00_OLSContent_GridChallenge1_GridEntry3']

    post_grid_next_element_css = '#ctl00_OLSContent_aNext'

    balance_element_css = ['#summaryDetails > tbody > tr > td.description > p:nth-child(1) > span']
    logout_button_element_css = '#page > div.headwrapper > div.logonPartial > div > div.loginButton > a'
    logout_confirm_element_css = '#btnConfirm'

    # Load site, input web ID, enters date of birth + clicks next.
    BankScraper.Core.start_and_open(URL)
    BankScraper.Core.input_to_element(web_id_input_element_css, web_id)
    dropdown_input(BankScraper, date_of_birth_dropdown_elements_css, date_of_birth)
    BankScraper.Core.click_element(cookies_button_css)
    BankScraper.Core.sleep(5)
    BankScraper.Core.click_element(next_button_css)


    # Reads the requirements and enters password characters.
    for i in range(0, len(password_required_elements_css)):
        word = BankScraper.Core.text_from_element(password_required_elements_css[i])
        needed_character = password_dictionary[word]
        BankScraper.Core.input_to_element(password_input_box_elements_css[i], needed_character)

    # Reads the requirements and enters grid card digits.
    for i in range(0, len(grid_required_elements_css)):
        grid_reference = BankScraper.Core.text_from_element(grid_required_elements_css[i])
        grid_input = grid_dict[grid_reference]
        BankScraper.Core.input_to_element(grid_input_box_elements_css[i], grid_input)


    # Sleeps, just because, and then continues.
    BankScraper.Core.sleep(2)
    BankScraper.Core.click_element(post_grid_next_element_css)


    # Scrape and return the balances
    account_balances = BankScraper.Core.scrape_balances(balance_element_css)

    # Coventry sulks if you don't log out.
    BankScraper.Core.click_element(logout_button_element_css)
    BankScraper.Core.sleep(3)
    BankScraper.Core.click_element(logout_confirm_element_css)
    BankScraper.Core.sleep(3)
    BankScraper.Core.driver.quit()

    return account_balances
