"""
Logs into Tesco.
Scrapes balances.
Two different modes depending on if the device is remembered or not.
"""

from banking_scrape import mail
import logging

logging.basicConfig(filename='banking_debug.log', level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')


def scrape_memorable_digits(BankScraper, memorable_element_css):
    """ Finds the digits needed from memorable information. """
    memorable_text = BankScraper.Core.text_from_element(memorable_element_css)
    memorable_digits = BankScraper.Core.numbers_from_string(memorable_text)
    return memorable_digits


def get_and_input_ota(BankScraper, text_me_element, input_ota_element, next_button_element):
    # Request text
    BankScraper.Core.click_element(text_me_element)

    # Read text message
    ota_code = mail.get_tesco_code()
    if not ota_code:
        return

    # Input text message, and click next.
    BankScraper.Core.input_to_element(input_ota_element, ota_code)
    BankScraper.Core.click_element(next_button_element)


def after_ota(BankScraper, input_password_element, password, recognise_device_no, next_button_element):
    # Input password, tell it to recognise, click next
    BankScraper.Core.input_to_element(input_password_element, password)
    BankScraper.Core.click_element(recognise_device_no)
    BankScraper.Core.click_element(next_button_element)


def main(BankScraper, login_tuple):
    """ """
    tesco_URL = 'https://www.tescobank.com/sss/auth'

    tesco_username, tesco_password, tesco_memorable = login_tuple

    remembered = False
    tesco_cookie_button_css = '#ensCloseBanner'
    tesco_username_element_css = '#login-uid'
    tesco_login_element_css = '#login-uid-submit-button'
    tesco_memorable_element_css = '#inputForm > div.form__panel > p:nth-child(2)'
    tesco_memorable_input_elements_css = ['#DIGIT1', '#DIGIT2', '#DIGIT3', '#DIGIT4', '#DIGIT5', '#DIGIT6']
    tesco_next_button_element_css = '#NEXTBUTTON'
    tesco_text_me_element_css = '#submit-ota-mobile'
    tesco_input_ota_element_css = '#OTA'
    tesco_input_password_element_css = '#PASSWORD'
    tesco_recognise_device_no_css = '#inputForm > div:nth-child(13) > div.form__item.form__group.no-bottom > fieldset > div:nth-child(3) > label'
    tesco_balance_elements_css = [
        '#current-account-product-1 > section > div.product-tile__overview > div.product-tile__body > dl:nth-child(1) > dd > span',
        '#current-account-product-2 > section > div.product-tile__overview > div.product-tile__body > dl:nth-child(1) > dd > span',
        '#savings-product-3 > section > div.product-tile__overview > div.product-tile__body > dl:nth-child(2) > dd',
        '#savings-product-4 > section > div.product-tile__overview > div.product-tile__body > dl:nth-child(2) > dd > span']

    BankScraper.Core.start_and_open(tesco_URL)

    # Tesco throws up a cookies button on first loading.
    # Not remembered.
    if not remembered:
        BankScraper.Core.click_element(tesco_cookie_button_css)

    # Input username + click login
    BankScraper.Core.input_to_element(tesco_username_element_css, tesco_username)
    BankScraper.Core.click_element(tesco_login_element_css)

    # Scrapes the memorable digits + works out which to input
    memorable_digits = scrape_memorable_digits(BankScraper, tesco_memorable_element_css)
    memorable_input = BankScraper.Core.get_memorable_characters(tesco_memorable, memorable_digits)

    # Inputs the 2 digits from the memorable information. -1 is because of 0-indexing.
    for i in range(0, 2):
        BankScraper.Core.input_to_element(tesco_memorable_input_elements_css[int(memorable_digits[i]) - 1],
                                          memorable_input[i])

    if remembered:
        BankScraper.Core.input_to_element(tesco_input_password_element_css, tesco_password)
    else:
        # Deal with the OTA code.
        BankScraper.Core.click_element(tesco_next_button_element_css)
        get_and_input_ota(BankScraper, tesco_text_me_element_css, tesco_input_ota_element_css,
                          tesco_next_button_element_css)

    BankScraper.Core.click_element(tesco_next_button_element_css)

    if not remembered:
        # Insert password
        after_ota(BankScraper, tesco_input_password_element_css, tesco_password, tesco_recognise_device_no_css,
                  tesco_next_button_element_css)

    # Scrape balances
    account_balances = BankScraper.Core.scrape_balances(tesco_balance_elements_css)
    BankScraper.Core.driver.quit()

    return account_balances
