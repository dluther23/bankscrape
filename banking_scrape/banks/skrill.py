"""
Logs into Skrill.
Scrapes balances.

Could add something to determine if it is the old or the new website.
"""

import logging

logging.basicConfig(filename='banking_debug.log', level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')


def main(BankScraper, login_tuple):
    """ """
    URL = 'https://account.skrill.com/login'

    username, password = login_tuple

    username_element_css = '#user_authentication_email'
    password_element_css = '#user_authentication_password'
    login_element_css = '#login_button'
    old_balance_elements_css = ['#account-panel > div > div.box.balance_panel.box-up.double-padding > div.info > span']
    new_balance_elements_css = ['body > pw-app > pw-header > div > div.user-space > div.available-balance.user-space-item > div > div.subtitle']

    # Username, password, click, scrape.
    BankScraper.Core.start_and_open(URL)
    BankScraper.Core.input_to_element(username_element_css, username)
    BankScraper.Core.input_to_element(password_element_css, password)
    BankScraper.Core.click_element(login_element_css)

    # Because Skrill is a bit slow at loading. 
    BankScraper.Core.sleep(5)
    try:
        account_balance = BankScraper.Core.scrape_balances(old_balance_elements_css)
    except:
        account_balance = BankScraper.Core.scrape_balances(new_balance_elements_css)

    BankScraper.Core.driver.quit()

    return account_balance
