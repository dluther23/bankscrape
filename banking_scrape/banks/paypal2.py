"""

Logs into Paypal

Scrapes balances.
"""

import logging

logging.basicConfig(filename='banking_debug.log', level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')


def main(BankScraper, login_tuple):
    """ """
    URL = 'https://www.paypal.com/uk/signin'

    username, password = login_tuple

    username_element_css = '#email'
    next_element_css = '#btnNext'
    password_element_css = '#password'
    login_element_css = '#btnLogin'
    contine_element_css = '[id="myaccount-button"]'
    balance_elements_css = ['#js_tourWalletModule > div.fiModule-currency-container.fiModule-currency_text > div > span.vx_h2.enforceLtr']

    BankScraper.Core.start_and_open(URL)

    # Inputs username, clicks next. Inputs password, clicks next.
    BankScraper.Core.input_to_element(username_element_css, username)
    BankScraper.Core.click_element(next_element_css)
    BankScraper.Core.sleep(1, 'letting password box become visible.')
    BankScraper.Core.input_to_element(password_element_css, password)
    BankScraper.Core.click_element(login_element_css)
    try:
        logging.info("Clicking proceed button.")
        BankScraper.Core.click_element(contine_element_css)
    except:
        logging.info("No proceed button")

    # Scrapes balance.
    account_balance = BankScraper.Core.scrape_balances(balance_elements_css)
    BankScraper.Core.driver.quit()
    return account_balance
