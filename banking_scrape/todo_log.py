"""
The logic is based on the idea that each script name only needs to be in the log once.
Even if somehow there end up multiple entries, the pop function removes all references so it won't matter.
This could be problematic if there are supposed to be double entries, in which case the pop logic needs re-working.

As far as the attributes, they should always be accurate as operations always being followed by a count/read.
A lot of the file reading is probably superfluous.
"""

import datetime
import logging
from pathlib import Path

logging.basicConfig(filename='banking_debug.log', level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')


class ToDoLog:
    """ Used to create a text-based to-do list. Keeps useful information in attributes too."""

    def __init__(self, audit='', filename=''):
        """ Sets init values. Filename can be set using a parameter, otherwise uses the current date."""
        self.date = (datetime.datetime.today().strftime('%Y-%m-%d'))
        self.log_length = 0
        self.next_bank = ''
        self.lines = []

        # Sets default filename if there isn't one
        if filename == '':
            self.filename = f'Log {self.date}.txt'
        else:
            self.filename = filename

        # If the file doesn't exist, create it.
        self.my_file = Path(self.filename)
        if not self.my_file.is_file():
            self.clear_log()
        # If the file existed and it's a continuing audit, read it in. Otherwise clear it..
        else:
            if audit == 'continue':
                self.read_all()
            else:
                self.clear_log()

    def clear_log(self):
        """ If there is no file, it creates one. If there is, it is cleared."""
        logging.info(f'Clearing log {self.filename}')
        with open(self.filename, 'w') as f:
            f.close()
        self.count_from_log()

    def count_from_log(self):
        """ Counts how many lines there are in the log. """
        with open(self.filename, 'r') as f:
            log_length = 0
            while True:
                line = f.readline()
                if line:
                    log_length += 1
                else:
                    break
        self.log_length = log_length

    def read_next(self):
        """ Return the next line. """
        with open(self.filename, 'r') as f:
            self.next_bank = f.readline().strip()
            if self.next_bank:
                logging.info(f'Read {self.next_bank} from log.')
            else:
                logging.info(f'No more banks.')
            f.close()
        self.count_from_log()

    def read_all(self):
        """ Reads each line into a list. Also calculates the length of the file."""
        # Reads each line into a list.
        with open(self.filename, 'r') as f:
            self.lines = f.readlines()
            f.close()
        self.count_from_log()
        return self.lines

    def append_to_log(self, script_name):
        """ Adds the given script to the log. Re-reads the file."""
        with open(self.filename, 'a') as f:
            logging.info(f'Appending {script_name} to log.')
            f.write(f'{script_name}\n')
            f.close()
        self.read_all()

    def pop_bank(self, script_name):
        """ Remove the script name from the log. """
        # Reads each line into a list.
        self.read_all()

        # Iterates through the lines looking for the name of the script to pop.
        with open(self.filename, 'w') as f:
            for line in self.lines:
                if line.strip() != f'{script_name}':
                    f.write(line)
                else:
                    logging.info(f'Popping {script_name} from log.')
            f.close()
        self.read_all()

    def push_failed_to_end(self, script_name):
        """ Pops a script and then appends it to the log. """
        # Reads each line into a list.
        self.read_all()
        self.pop_bank(script_name)
        self.append_to_log(script_name)
