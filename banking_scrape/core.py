from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium import webdriver
from pathlib import Path
import datetime
import inspect
import logging
import time
import re

logging.basicConfig(filename='banking_debug.log', level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')


class CoreTools:
    def __init__(self):
        self.URL = ''
        self.driver = ''
        self.element_text = ''
        self.account_balances = []
        self.memorable_characters = []
        self.timeout = 5

    def start_and_open(self, URL, special=False):
        """ Loads up the browser. Opens the URL """
        self.URL = URL
        if special:
            # For when I want to use my 'normal' Chrome with cookies.
            logging.info("Special mode activated.")
            opts = webdriver.ChromeOptions()
            opts.add_argument("user-data-dir=/Users/obow/Library/Application Support/Google/Chrome/")
            self.driver = webdriver.Chrome(chrome_options=opts)
        else:
            pass
            # Otherwise use a fresh instance of Chrome.
            self.driver = webdriver.Chrome()
        self.driver.get(self.URL)
        logging.info(f"Opened {URL}.")
        return self.driver

    def input_to_element(self, element_css: object, input_text: object):
        """ Sends given input to given element """
        self.wait_for_presence(element_css)
        element = self.driver.find_element_by_css_selector(element_css)
        element.send_keys(input_text)

    def input_to_elements(self, element_css_list, input_text_list):
        """ Sends given input to given elements """
        for i in range(0, len(element_css_list)):
            self.wait_for_presence(element_css_list[i])
            element = self.driver.find_element_by_css_selector(element_css_list[i])
            element.send_keys(input_text_list[i])

    def click_element(self, element_css):
        """ Clicks given element """
        self.wait_for_clickable(element_css)
        self.driver.find_element_by_css_selector(element_css).click()

    def text_from_element(self, element_css):
        """ Extracts text from given element """
        self.wait_for_presence(element_css)
        self.element_text = self.driver.find_element_by_css_selector(element_css).text
        return self.element_text

    def scrape_balances(self, elements):
        logging.info("scraping balances")
        account_balances = []
        for i in range(0, len(elements)):
            self.wait_for_presence(elements[i])
            account_balances.append(self.balance_from_string(self.text_from_element(elements[i])))
            logging.info(f'Balance - {account_balances[i]}')
        self.account_balances = account_balances
        return self.account_balances

    def get_memorable_characters(self, memorable_info, characters_required):
        """ Looks through the input memorable info and puts the characters in order in a list."""
        # -1 is used because the real world doesn't use 0 indexing.
        self.memorable_characters = []
        for i in characters_required:
            self.memorable_characters.append(memorable_info[int(i)-1])
        return self.memorable_characters

    def select_visible_from_element(self, element_css, text):
        """ Selects given text from given dropdown element """
        self.wait_for_presence(element_css)
        Select(self.driver.find_element_by_css_selector(element_css)).select_by_visible_text(text)

    def sleep(self, seconds, reason="idling."):
        """ Sleeps and prints a reason"""
        logging.info(f"Sleeping for {seconds} while {reason}.")
        time.sleep(seconds)

    def wait_for_presence(self, element_value, by_what=By.CSS_SELECTOR):
        """ Used to ensure that an element is present before continuing. """
        # https://seleniumhq.github.io/selenium/docs/api/py/webdriver/selenium.webdriver.common.by.html for By
        # references. http://selenium-python.readthedocs.io/waits.html
        try:
            element_present = EC.presence_of_element_located((by_what, element_value))
            WebDriverWait(driver=self.driver, timeout=self.timeout).until(element_present)
        except Exception as e:
            logging.error(f"{element_value} not present.")
            name = inspect.stack()[0][3]
            self.error_message(e, name)

    def wait_for_clickable(self, element_value, by_what=By.CSS_SELECTOR):
        """ Used to ensure an element is clickable before continuing. """
        # https://seleniumhq.github.io/selenium/docs/api/py/webdriver/selenium.webdriver.common.by.html for By
        # references.
        try:
            element_present = EC.element_to_be_clickable((by_what, element_value))
            WebDriverWait(driver=self.driver, timeout=self.timeout).until(element_present)
        except Exception as e:
            logging.error(f"{element_value} not clickable.")
            name = inspect.stack()[0][3]
            self.error_message(e, name)

    def error_message(self, e, name):
        """ Log the error message. Name should be function where the error happened. """
        logging.error(name)
        logging.error(str(e))
        self.screenshot(f"{name}")

    def screenshot(self, name):
        """ Takes a screenshot. Stores it in screenshots/{date}/{calling_function}/ """
        calling_function = inspect.stack()[1][3]
        now = datetime.datetime.now()
        string_time = now.strftime('%H-%M-%S')
        date_string = now.strftime("%Y-%m-%d")

        folder_name = f'screenshot/{date_string}/{calling_function}'
        # Creates the folder if it doesn't exist.
        Path(folder_name).mkdir(parents=True, exist_ok=True)
        screenshot_folder = Path(folder_name)
        name = screenshot_folder / f'{string_time} - {name}.png'
        # Needs to be converted to a string from a path to work with save_screenshot()
        string_name = str(name)
        logging.info(f"Taking screenshot {name}")
        self.driver.save_screenshot(string_name)

    def number_from_string(self, string_in):
        """ Takes a string, returns the first digit. """
        # d+ means 10. d would split it into 1 and 0 """
        regex = re.compile(r'(\d+)')
        found = (regex.findall(string_in))
        logging.info(f'Extracted {found[0]}.')
        return found[0]

    def numbers_from_string(self, string_in):
        """ Takes a string, returns numbers. """
        # d+ means 2, 3, 10. d would be 2, 3, 1, 0 """
        regex = re.compile(r'(\d+)')
        found = (regex.findall(string_in))
        logging.info(f'Extracted {found}.')
        return found

    def balance_from_string(self, string_in):
        """ Uses regex to extract the balance from a string"""
        regex = re.compile(r'(\d+,\d+.\d+)')
        found = (regex.findall(string_in))
        if not found:
            regex = re.compile(r'(\d+.\d+)')
            found = (regex.findall(string_in))
        if not found:
            regex = re.compile(r'(\d+)')
            found = (regex.findall(string_in))
        if found:
            # Returns the first number found. Strips comma so it can be stored as float.
            stripped_found = found[0].replace(',', '')
            return stripped_found
        else:
            # If there is no number found.
            logging.info(f'Unable to extract balance from {string_in}.')
            return 'No number found.'

    def work_out_digits(self, elements_list):
        """ Reads the 3 elements and makes a string of the digits"""
        # Change to a list.
        digits_needed = []
        for i in range(1, len(elements_list)+1):
            element_css = elements_list[i-1]
            digits_needed += self.numbers_from_string(self.text_from_element(element_css))
        return digits_needed


def sleep(seconds, reason="idling."):
    """ Sleeps and prints a reason"""
    logging.info(f"Sleeping for {seconds} while {reason}.")
    time.sleep(seconds)
