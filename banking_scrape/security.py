"""
Uses Fernet to encode.
Stores in Keyring.
Has a separate encrypt and decrpyt function which look similar because it's easier to code and access from outside.
"""

from cryptography.fernet import Fernet
import keyring
import pickle
import logging

logging.basicConfig(filename='banking_debug.log', level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')


def generate_key():
    """Generates a Fernet Key and stores it in OS Keyring. Is a one-off function."""
    logging.info('Generating a new Fernet key.')
    my_keyring = keyring.get_keyring()
    key = Fernet.generate_key()
    key = str(key)
    my_keyring.set_password('My Fernet', 'Banking Key', key)


def get_password_from_keyring(username='', which_key='banking'):
    """ Pulls the key out of the Keyring. Defaults to the banking key, but can be used for other passwords."""
    my_keyring = keyring.get_keyring()
    if which_key == 'banking':
        # slices the string to remove the b'' as otherwise it goes wrong.
        key = my_keyring.get_password('My Fernet', 'Banking Key')[1:-1]
    else:
        key = my_keyring.get_password(username, which_key)

    return key


def set_password_in_keyring(username, account, password):
    """ Used to set a password for the mail setup."""
    # For GMail account = 'My Gmail', db = 'PostgreSQL'
    my_keyring = keyring.get_keyring()
    my_keyring.set_password(username, account, password)
    logging.info('Stored Gmail password in keychain.')


def make_fernet_object(key):
    """ Creates a Fernet object with the key. Needed to encrypt + decrypt. """
    f = Fernet(key)

    return f


def decrypt_tuple(password_token):
    """ Takes the token stored in the database and returns a decrypted password tuple. """
    try:
        tuple_to_return = decrypt(password_token)
        return tuple_to_return
    except Exception as e:
        logging.error('Issue decrpyting token.')
        logging.error(e)


def decrypt(string_token):
    """ Encodes the string as a byte, decrpyts the byte token, depickles it and returns the tuple contents. """
    key = get_password_from_keyring()
    f = make_fernet_object(key)
    byte_token = str.encode(string_token)
    decrypted_pickle = f.decrypt(byte_token)
    depickled_tuple = pickle.loads(decrypted_pickle)

    return depickled_tuple


def encrypt(object_to_encrypt):
    """ Takes an object, pickles it, encrypts it into a token and then stores it as a string for the DB. """
    key = get_password_from_keyring()
    f = make_fernet_object(key)
    pickled_object = pickle.dumps(object_to_encrypt)
    token = f.encrypt(pickled_object)
    string_token = token.decode()

    return string_token
